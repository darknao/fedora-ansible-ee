# fedora-ansible-ee

Custom Ansible Execution Environment for Fedora Infra.

## Build the image locally

First, [install ansible-builder](https://ansible-builder.readthedocs.io/en/stable/installation/).

Then run the following command from the root of this repo:

```bash
$ ansible-builder build -t fedora-ansible-ee
```

## Use this Execution Environment

Available Execution Environment image tags:
```
registry.gitlab.com/darknao/fedora-ansible-ee:latest
```

## What's inside?

Base image: **fedora-minimal**  
Ansible version: **2.14.2**  
Installed Python libraries:
```
libvirt
jmespath
kubernetes
fedora-messaging
```

Installed Collections:
```
Collection                Version
------------------------- -------
ansible.posix             1.5.4
community.docker          3.4.7
community.general         7.1.0
community.libvirt         1.2.0
community.mysql           3.7.2
community.rabbitmq        1.2.3
community.postgresql      3.0.0
containers.podman         1.10.2
fedora.linux_system_roles 1.42.1
freeipa.ansible_freeipa   1.11.0
```
